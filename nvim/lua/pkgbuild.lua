---@diagnostic disable: undefined-global

local M = {}

function M.increment()
	for i = 1,vim.api.nvim_buf_line_count(0) do
		local line = vim.api.nvim_buf_get_lines(0, i-1, i, true)[1]
		local pkgrel = string.match(line, "^pkgrel=(.*)")
		if pkgrel then
			local rel = tonumber(pkgrel) + 1
			vim.api.nvim_buf_set_lines(0, i-1, i, true, {"pkgrel="..rel})
			break
		end
	end
	vim.cmd [[update]]
end

function M.makepkg()
	vim.cmd [[
	update
	!makepkg -fcrs --noconfirm
	]]
end

function M.updpkgsums()
	for i = 1,vim.api.nvim_buf_line_count(0) do
		local line = vim.api.nvim_buf_get_lines(0, i-1, i, true)[1]
		if string.match(line, "sums=%(") then
			vim.cmd [[
			update
			!updpkgsums
			edit
			]]
			return
		end
	end
end

function M.push()
	vim.cmd [[
	!custom add *.pkg.* && rm -f *.pkg.*
	]]
end

function M.install()
	vim.cmd [[
	term custom install
	]]
end

function M.release(install)
	M.increment()
	M.updpkgsums()
	M.makepkg()
	M.push()
	if install then
		M.install()
	end
end

function M.setup()
	vim.cmd [[
	augroup TermOpen
	au!
	au TermOpen * startinsert
	augroup END

	command! PkgIncrement :lua require("pkgbuild").increment()
	command! PkgInstall :lua require("pkgbuild").install()
	command! PkgMake :lua require("pkgbuild").makepkg()
	command! PkgPush :lua require("pkgbuild").push()
	command! -bang PkgRelease :lua require("pkgbuild").release("<bang>" == "!")
	command! PkgUpdpkgsums :lua require("pkgbuild").updpkgsums()
	]]
end

return M
