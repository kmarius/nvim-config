return {
	s("once", {t("#pragma once")}),
	parse("inc", "#include <$0>"),
	parse("Inc", "#include \"$0\""),

	s("if", fmt([[
	if ({}) {{
		{}
	}}
	]], {i(1), vi(2, {indent="\t"})})),

	s("ife", fmt([[
	if ({}) {{
		{}
	}} else {{
		{}
	}}
	]], {i(1), vi(2, {indent="\t"}), i(3)})),

	s("else", fmt([[
	else {{
		{}
	}}
	]], {vi(1, {indent="\t"})})),

	s("el", fmt([[
	else {{
		{}
	}}
	]], {vi(1, {indent="\t"})})),

	s("while", fmt([[
	while ({}) {{
		{}
	}}
	]], {i(1), vi(2, {indent="\t"})})),

	s("do", fmt([[
	do {{
		{}
	}} while ({});
	]], {vi(1, {indent="\t"}), i(2)})),

	s("switch", fmt([[
	switch ({}) {{
		{}
	}}
	]], {i(1), i(0)})),
}
