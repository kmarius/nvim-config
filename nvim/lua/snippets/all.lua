local cms_suff = require("snippets.util.fn").cms_suff
local cms_pre = require("snippets.util.fn").cms_pre
local shell = require("snippets.util.fn").shell

-- local ok, jsregex = pcall(require, "jsregexp")
-- local transformer
-- if not ok then
-- 	transformer = function(_, _, _)
-- 		return function(arg) return arg end
-- 	end
-- else
-- 	transformer = jsregex.transformer
-- end

-- local function wrapping_transformer(reg, fmt, flags)
-- 	local trafo, err = transformer(reg, fmt, flags)
-- 	if err then
-- 		return nil, err
-- 	end
-- 	return function(arg)
-- 		if type(arg) ~= "table" then
-- 			arg = {arg}
-- 		end
-- 		local str = trafo(table.concat(arg, "\n"))
-- 		local res = {}
-- 		print(table.concat(arg, "\n"), " -> ", str)
-- 		for line in string.gmatch(str, "([^\n]+)") do
-- 			table.insert(res, line)
-- 		end
-- 		return res
-- 	end
-- end

-- local function tr(var, reg, fmt, flags)
-- 	local trafo, err = wrapping_transformer(reg, fmt, flags)
-- 	if err then
-- 		error(err)
-- 	end
-- 	if type(var) == "string" then
-- 		return f(function(_, snip) return trafo(snip.env[var]) end, {})
-- 	elseif type(var) == "number" then
-- 		return f(function(text, _) return trafo(text[1]) end, {var})
-- 	else
-- 		--
-- 	end
-- end


return {
	s({trig="test"}, {
		i(1),
		t({"", "", ""}),
		-- tr(1, "([äöüÄÖÜ]*)|(\\w+)|(\n)", "${1:/upcase}${2:/upcase}${3:+_}", "g"),
		-- tr(1, "([äöüÄÖÜ]*)|(\\w+)", "${1:/upcase}${2:+_}", "g"),
		-- tr(1, "ö", "", "g"),
		-- t({"", ""}),
		-- tr("TM_FILENAME", "(\\w+)|(\\W+)", "${1:/upcase}${2:+_}", "g"),
		-- t({"", ""}),
		-- tr("TM_SELECTED_TEXT", "([äöüÄÖÜ])|(\\w+)|(\n)", "${1:/downcase}${2:/upcase}${3:+_}", "g"),
	}),

	s("fold", fmt([[
	{} {} {{{{{{ {}
	{}
	{} }}}}}} {}
	]],
	{f(cms_pre, {}), i(1), f(cms_suff, {}),
	vi(2),
	f(cms_pre, {}), f(cms_suff, {})})),

	s("todo", fmt("{} TODO: {} (on {}) {}", {f(cms_pre, {}), vi(1), f(shell, {}, {user_args={"date --iso-8601"}}), f(cms_suff, {})})),
	s("date", {f(shell, {}, "date --iso-8601")}),
	s("lorem", {t({
		"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod",
		"tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At",
		"vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd",
		"gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum",
		"dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor",
		"invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero",
		"eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no",
		"sea takimata sanctus est Lorem ipsum dolor sit amet.",
	})}),
}
