return {
	s("if", {t("if "), i(1), t({"; then", "\t"}), vi(2, {indent="\t"}), t({"", "fi"})}),
	s("ife", {t("if"), i(1), t({"; then", "\t"}), vi(2, {indent="\t"}), t({"", "else", "\t"}), i(3), t({"", "fi"})}),
	s("for", {t("for "), i(1, "arg"), t(" in "), i(2, '"$@"'), t({"; do", ""}), vi(3, {indent="\t"}), t({"", "done"})}),
	s("wh", {t("while "), i(1), t({"; do", ""}), vi(2, {indent="\t"}), t({"", "done"})}),
	s("while", {t("while "), i(1), t({"; do", ""}), vi(2, {indent="\t"}), t({"", "done"})}),
	s("function", {i(1),  t({"() {", ""}), vi(2, {indent="\t"}), t({"", "}"})}),
	s("fun", {i(1),  t({"() {", ""}), vi(2, {indent="\t"}), t({"", "}"})}),
}
