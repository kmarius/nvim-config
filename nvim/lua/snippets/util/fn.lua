local M = {}

function M.shell(_, _, command)
	local file = io.popen(command, "r")
	local res = {}
	for line in file:lines() do
		table.insert(res, line)
	end
	return res
end

function M.cms_pre()
	return string.gsub(vim.o.cms, "^(.-)%s*%%s.*", "%1")
end

function M.cms_suff()
	return string.gsub(vim.o.cms, "^.*%%s%s*(.*)$", "%1")
end

function M.leading_whitespace(s)
	return string.match(s, "^(%s*)") or ""
end

-- Returns the indentation (i.e. the leading whitespace) of the first previous
-- line found containing `tok`, starting at the trigger line.
function M.find_indent(args, tok)
	local lnum = args[1].env.TM_LINE_INDEX
	local lines = vim.api.nvim_buf_get_lines(0, 0, lnum, false)
	for i=lnum,1,-1 do
		if string.find(lines[i], tok) then
			return M.leading_whitespace(lines[i])
		end
	end
	return ""
end

-- Returns a function that returns the string `ins` if the given tabstop
-- is non-empty.
function M.ifn(ins)
	return function(args)
		return args[1][1] ~= "" and ins or ""
	end
end

-- Returns a function that returns the string `ins` if the give tabstop
-- contains `tok`.
function M.ifc(tok, ins)
	return function(args)
		return args[1][1]:find(tok) and ins or ""
	end
end

function M.fnameupper()
	local file = string.upper(vim.fn.expand("%p"))
	return string.gsub(file, "%.", "_")
end

-- Returns capture group i, to be used as: f(capture, {}, i)
function M.capture(args, i)
	return args[1][i]
end

return M
