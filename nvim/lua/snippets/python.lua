return {
	s("if", {t("if "), i(1), t({":", ""}), vi(2, {indent="\t"})}),
	s("ife", {t("if "), i(1), t({":", ""}), vi(2, {indent="\t"}), t({"", "else:", "\t"}), i(0)}),
	s("for", {t("for "), i(1), t(" in "), i(2), t({":", ""}), vi(3, {indent="\t"})}),
	s("fore", {t("for "), i(1, "i"), t(", "), i(2, "e"), t(" in enumerate("), i(3), t({"):", ""}), vi(4, {indent="\t"})}),
	s("method", {t("def "), i(1), t("(self"), i(2), t({"):", ""}), vi(3, {indent="\t"})}),
	s("fun", {t("def "), i(1), t("("), i(2), t({"):", ""}), vi(3, {indent="\t"})}),
	s("func", {t("def "), i(1), t("("), i(2), t({"):", ""}), vi(3, {indent="\t"})}),
	s("function", {t("def "), i(1), t("("), i(2), t({"):", ""}), vi(3, {indent="\t"})}),
	s("lambda", {t("lambda "), i(1), t(": "), vi(0)}),
	s("lam", {t("lambda "), i(1), t(": "), vi(0)}),
}
