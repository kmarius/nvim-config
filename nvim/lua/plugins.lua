---@diagnostic disable: undefined-global

local fn = vim.fn

local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

if fn.empty(fn.glob(install_path)) > 0 then
	fn.system({"git", "clone", "https://github.com/wbthomason/packer.nvim", install_path})
	vim.cmd "packadd packer.nvim"
end

vim.api.nvim_create_autocmd("BufWritePost", {
	group = vim.api.nvim_create_augroup("packercompile", {clear = true}),
	pattern = "plugins.lua",
	command = "source <afile> | PackerCompile",
})

return require("packer").startup(function(use)

	use "wbthomason/packer.nvim"

	use_rocks 'jsregexp'

	-- use {
	-- 	'kyazdani42/nvim-tree.lua',
	-- 	disable = true,
	-- 	requires = {
	-- 		'kyazdani42/nvim-web-devicons',
	-- 	},
	-- 	tag = 'nightly',
	-- 	config = function()
	-- 		require("nvim-tree").setup()
	-- 	end
	-- }

	use {
		'phaazon/hop.nvim',
		branch = 'v2',
		config = function()
			require('hop').setup({
				keys = 'etovxqpdygfblzhckisuran'
			})
			vim.api.nvim_set_keymap('n', 'gh', ":HopWordMW<cr>", {})
		end
	}

	use {
		"lukas-reineke/indent-blankline.nvim",
		after = "nord-vim",
		config = function()
			vim.cmd [[highlight IndentBlanklineChar ctermfg=237 cterm=nocombine gui=nocombine]]
			vim.cmd [[highlight IndentBlanklineSpaceChar ctermfg=237 cterm=nocombine gui=nocombine]]
			require("indent_blankline").setup({})
		end
	}

	use {
		"akinsho/bufferline.nvim",
		tag = "v2.*",
		requires = "kyazdani42/nvim-web-devicons",
		config = function()
			require("bufferline").setup()
		end
	}

	use {
		"mfussenegger/nvim-dap",
		ft = {"c"},
		config = function()
			local dap = require('dap')
			dap.adapters.lldb = {
				type = 'executable',
				command = '/usr/bin/lldb-vscode', -- adjust as needed, must be absolute path
				name = 'lldb'
			}

			dap.configurations.cpp = {
				{
					name = 'Launch',
					type = 'lldb',
					request = 'launch',
					program = function()
						return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
					end,
					cwd = '${workspaceFolder}',
					stopOnEntry = false,
					args = {},

					-- 💀
					-- if you change `runInTerminal` to true, you might need to change the yama/ptrace_scope setting:
					--
					--    echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
					--
					-- Otherwise you might get the following error:
					--
					--    Error on launch: Failed to attach to the target process
					--
					-- But you should be aware of the implications:
					-- https://www.kernel.org/doc/html/latest/admin-guide/LSM/Yama.html
					-- runInTerminal = false,
				},
			}

			dap.configurations.c = dap.configurations.cpp
			dap.configurations.rust = dap.configurations.cpp
		end,
	}

	use {
		"rcarriga/nvim-dap-ui",
		requires = {"mfussenegger/nvim-dap"},
		ft = {"c"},
	}

	use "lambdalisue/suda.vim"

	use {
		"ldelossa/litee.nvim",
		-- not working with neither ccls nor sumneko
		disable = true,
		config = function()
			require("litee").setup({})
		end
	}

	use {
		"arcticicestudio/nord-vim",
		config = function()
			vim.cmd "colorscheme nord"
			vim.cmd "hi DiffAdd cterm=none"
			vim.cmd "hi DiffChange cterm=none"
			vim.cmd "hi DiffDelete cterm=none"
			vim.cmd "hi DiffText cterm=none"
		end,
	}

	use {
		"folke/lsp-colors.nvim",
		after = "nord-vim",
	}

	use {
		"folke/trouble.nvim",
		requires = "kyazdani42/nvim-web-devicons",
		config = function()
			require("trouble").setup({})
		end
	}

	use {
		"ray-x/lsp_signature.nvim",
		config = function()
			require("lsp_signature").setup({
				doc_lines = 0,
				floating_window = false,
				hint_prefix = "param: ",
				toggle_key = "<c-k>",
			})
		end
	}

	use {
		"neovim/nvim-lspconfig",
		requires = {"ray-x/lsp_signature.nvim"},
		config = function()
			local lspconfig = require("lspconfig")
			local util = require('lspconfig.util')

			local capabilities = vim.lsp.protocol.make_client_capabilities()
			local ok, cmp = pcall(require, 'cmp_nvim_lsp')
			if ok then
				capabilities = cmp.update_capabilities(capabilities)
			end

			local opts = {noremap=true, silent=true}
			local map = vim.api.nvim_set_keymap

			local function on_attach(client, bufnr)
				local handlers = vim.lsp.handlers
				local with = vim.lsp.with
				handlers["textDocument/hover"] = with(vim.lsp.handlers.hover, {border = "single"})
				handlers["textDocument/signatureHelp"] =  with(vim.lsp.handlers.signature_help, {border = "single"})
				handlers["textDocument/publishDiagnostics"] = with(vim.lsp.diagnostic.on_publish_diagnostics, {update_in_insert = false})
			end

			lspconfig.on_attach = on_attach

			map("n", "glS", [[<cmd>lua vim.lsp.stop_client(vim.lsp.get_active_clients())<CR>]], opts)
			map("n", "gld", [[<cmd>lua vim.lsp.buf.definition()<CR>]], opts)
			map("n", "glD", [[<cmd>lua vim.lsp.buf.declaration()<CR>]], opts)
			map("n", "glt", [[<cmd>lua vim.lsp.buf.type_definition()<CR>]], opts)
			map("n", "glr", [[<cmd>lua vim.lsp.buf.rename()<CR>]], opts)
			map("n", "glh", [[<cmd>lua vim.lsp.buf.hover()<CR>]], opts)
			map("n", "K", [[<cmd>lua vim.lsp.buf.hover()<CR>]], opts)
			map("n", "gls", [[<cmd>lua vim.lsp.buf.signature_help()<CR>]], opts)
			map('n', 'gli', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
			map('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
			-- map('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
			-- map('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
			-- map('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
			map('n', 'glR', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
			map('n', 'gll', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
			map('n', 'gln', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
			map('n', 'glp', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
			-- map('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
			-- map('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

			-- local servers = {'pylsp', 'ccls'}
			local servers = {'pylsp'}
			for _, server in ipairs(servers) do
				lspconfig[server].setup({
					capabilities = capabilities,
					on_attach = on_attach,
					flags = {
						debounce_text_changes = 150,
					}
				})
			end
			lspconfig.sumneko_lua.setup({
				capabilities = capabilities,
				on_attach = on_attach,
				cmd = {"lua-language-server"},
				flags = { debounce_text_changes = 150, },
				root_dir = util.root_pattern('.luarc.lua', '.git', '.luarc.json'),
				settings = {
					Lua = {
						runtime = {version = 'LuaJIT'},
						telemetry = {enable = false},
					},
				},
				on_init = function(client)
					local root_dir = client.config.root_dir or "."
					local f = loadfile(root_dir .. "/.luarc.lua")
					if f then
						local t = f()
						if t then
							local settings = vim.tbl_extend("force", client.config.settings, t)
							client.config.settings = settings
							client.notify("workspace/didChangeConfiguration")
						end
					end
				end,
			})
		end,
	}

	use {
		"p00f/clangd_extensions.nvim",
		config = function()
			require("clangd_extensions").setup({
				server = {
					cmd = {
						"clangd",
						"--background-index",
						"--suggest-missing-includes",
					},
				},
				extensions = {
					inlay_hints = {
						only_current_line = true,
					}
				}
			})
		end,
	}

	use {
		"weilbith/nvim-code-action-menu",
		config = function()
			vim.g.code_action_menu_show_details = false

			local map = vim.api.nvim_set_keymap
			local opts = {noremap=true, silent=true}
			map("n", "gla", [[<cmd>CodeActionMenu<cr>]], opts)
			map("n", "<a-enter>", [[<cmd>CodeActionMenu<cr>]], opts)
		end
	}

	use {
		"L3MON4D3/LuaSnip",
		config = function()
			local ls = require("luasnip")
			local types = require("luasnip.util.types")
			ls.config.setup({
				history = true,
				updateevents = "TextChanged,TextChangedI",
				store_selection_keys = "<Tab>",
				enable_autosnippets = false,
				ext_opts = {
					[types.choiceNode] = {
						active = { virt_text = { { '●', 'Error' } } },
					},
					[types.insertNode] = {
						active = { virt_text = { { '●', 'String' } } },
					},
				},
				snip_env = {
					s = require("luasnip.nodes.snippet").S,
					sn = require("luasnip.nodes.snippet").SN,
					t = require("luasnip.nodes.textNode").T,
					f = require("luasnip.nodes.functionNode").F,
					i = require("luasnip.nodes.insertNode").I,
					c = require("luasnip.nodes.choiceNode").C,
					d = require("luasnip.nodes.dynamicNode").D,
					r = require("luasnip.nodes.restoreNode").R,
					l = require("luasnip.extras").lambda,
					rep = require("luasnip.extras").rep,
					p = require("luasnip.extras").partial,
					m = require("luasnip.extras").match,
					n = require("luasnip.extras").nonempty,
					dl = require("luasnip.extras").dynamic_lambda,
					fmt = require("luasnip.extras.fmt").fmt,
					fmta = require("luasnip.extras.fmt").fmta,
					conds = require("luasnip.extras.expand_conditions"),
					types = require("luasnip.util.types"),
					events = require("luasnip.util.events"),
					parse = require("luasnip.util.parser").parse_snippet,
					ai = require("luasnip.nodes.absolute_indexer"),
					vi = require("snippets.util.nodes").visual_insert,
				},
			})

			-- TODO: write to .luarc.json on compile? (on 2022-03-22)

			require("luasnip").filetype_set("cpp", {"cpp", "c"})

			require("luasnip.loaders.from_lua").lazy_load({paths="/home/marius/.config/nvim/lua/snippets"})

			local map = vim.api.nvim_set_keymap
			map("i", "<Tab>", [[luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>']], {expr=true, silent=true})
			map("i", "<s-tab>", [[<cmd>lua require'luasnip'.jump(-1)<Cr>]], {silent=true, noremap=true})
			map("s", "<Tab>", [[<cmd>lua require("luasnip").jump(1)<Cr>]], {silent=true, noremap=true})
			map("s", "<s-Tab>", [[<cmd>lua require("luasnip").jump(-1)<Cr>]], {silent=true, noremap=true})
			map("i", "<c-j>", [[<cmd>lua require("luasnip").jump(1)<Cr>]], {silent=true, noremap=true})
			map("i", "<c-k>", [[<cmd>lua require("luasnip").jump(-1)<Cr>]], {silent=true, noremap=true})
			map("s", "<c-j>", [[<cmd>lua require("luasnip").jump(1)<Cr>]], {silent=true, noremap=true})
			map("s", "<c-k>", [[<cmd>lua require("luasnip").jump(-1)<Cr>]], {silent=true, noremap=true})

			map("i", "<c-e>", [[luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>']], {expr=true, silent=true})
			map("s", "<c-e>", [[luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>']], {expr=true, silent=true})
		end
	}

	use {
		"hrsh7th/nvim-cmp",
		requires = {
			{"hrsh7th/cmp-nvim-lsp", after = "nvim-cmp"},
			{"hrsh7th/cmp-path", after = "nvim-cmp"},
		},
		after = {"LuaSnip"},
		config = function()
			local cmp = require("cmp")
			local luasnip = require("luasnip")
			cmp.setup({
				snippet = {
					expand = function(args)
						luasnip.lsp_expand(args.body)
					end
				},
				mapping = {
					["<C-d>"] = cmp.mapping.scroll_docs(-4),
					["<C-f>"] = cmp.mapping.scroll_docs(4),
					["<C-Space>"] = cmp.mapping.complete(),
					["<C-e>"] = cmp.mapping.close(),
					["<CR>"] = cmp.mapping.confirm({
						behavior = cmp.ConfirmBehavior.Replace,
						select = true,
					}),
					['<Tab>'] = function(fallback)
						if luasnip.expand_or_jumpable() then
							luasnip.expand_or_jump()
						elseif cmp.visible() then
							cmp.select_next_item()
						else
							fallback()
						end
					end,
					['<S-Tab>'] = function(fallback)
						if luasnip.jumpable(-1) then
							luasnip.jump(-1)
						elseif cmp.visible() then
							cmp.select_prev_item()
						else
							fallback()
						end
					end,
				},
				sources = {
					{ name = "path" },
					{ name = "nvim_lsp" },
				}
			})
			vim.opt.completeopt = "menuone,noselect"
		end
	}


	use {
		"nvim-treesitter/nvim-treesitter",
		requires = {
			'nvim-treesitter/nvim-treesitter-refactor',
			'nvim-treesitter/nvim-treesitter-textobjects',
		},
		run = ":TSUpdate",
		config = function()
			require('nvim-treesitter.configs').setup{
				ensure_installed = {"lua", "c", "bash", "cmake", "cpp", "fish", "go", "java", "latex", "make", "python", "rust", "vim"},
				highlight = {
					enable = true,
				},
				incremental_selection = {
					enable = false,
					keymaps = {
						init_selection = 'gnn',
						node_incremental = 'grn',
						scope_incremental = 'grc',
						node_decremental = 'grm',
					},
				},
				indent = {
					enable = false,
				},
				textobjects = {
					select = {
						enable = true,
						lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
						keymaps = {
							-- You can use the capture groups defined in textobjects.scm
							['af'] = '@function.outer',
							['if'] = '@function.inner',
							['ac'] = '@class.outer',
							['ic'] = '@class.inner',
						},
					},
					move = {
						enable = true,
						set_jumps = true, -- whether to set jumps in the jumplist
						goto_next_start = {
							[']m'] = '@function.outer',
							[']]'] = '@class.outer',
						},
						goto_next_end = {
							[']M'] = '@function.outer',
							[']['] = '@class.outer',
						},
						goto_previous_start = {
							['[m'] = '@function.outer',
							['[['] = '@class.outer',
						},
						goto_previous_end = {
							['[M'] = '@function.outer',
							['[]'] = '@class.outer',
						},
					},
				},
			}
			local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
			parser_config.org = {
				install_info = {
					url = 'https://github.com/milisims/tree-sitter-org',
					revision = 'main',
					files = {'src/parser.c', 'src/scanner.cc'},
				},
				filetype = 'org',
			}
			require('nvim-treesitter.configs').setup({
				-- If TS highlights are not enabled at all, or disabled via `disable` prop, highlighting will fallback to default Vim syntax highlighting
				highlight = {
					enable = true,
					disable = {'org'}, -- Remove this to use TS highlighter for some of the highlights (Experimental)
					additional_vim_regex_highlighting = {'org'}, -- Required since TS highlighter doesn't support all syntax features (conceal)
				},
				ensure_installed = {'org'}, -- Or run :TSUpdate org
			})
		end
	}

	use {
		"sbdchd/neoformat",
		ft = {"c", "cpp"},
		disable = true,
		config = function()
			local clangformat = {
				exe = "clang-format",
				args = {
					[[--style="{
						BasedOnStyle: LLVM,
						ColumnLimit: 80,
						IndentWidth: 8,
						BreakBeforeBraces: Linux,
						IndentCaseLabels: false,
						UseTab: Always
					}"]],
				},
			}
			vim.g.neoformat_c_clangformat = clangformat
			vim.g.neoformat_cpp_clangformat = clangformat
			vim.g.neoformat_enabled_cpp = {"clangformat"}
			vim.g.neoformat_enabled_c = {"clangformat"}
		end,
	}

	use {
		"folke/lua-dev.nvim",
		disable = true,
		requires = "neovim/nvim-lspconfig",
		ft = "lua",
		config = function()
			local luadev = require("lua-dev").setup({})
			require('lspconfig').sumneko_lua.setup(luadev)
		end
	}

	use {
		"ludovicchabant/vim-gutentags",
		ft = {"c", "cpp", "rust"},
	}

	use {
		'nvim-telescope/telescope.nvim',
		requires = {'nvim-lua/plenary.nvim'},
		config = function()
			local map = vim.api.nvim_set_keymap
			require('telescope').setup{
				defaults = {
					mappings = {
						i = {
							['<C-u>'] = false,
							['<C-d>'] = false,
						},
					},
				},
			}
			map('n', '<leader><space>', [[<cmd>lua require('telescope.builtin').buffers()<CR>]], { noremap = true, silent = true })
			map('n', '<leader>sf', [[<cmd>lua require('telescope.builtin').find_files({previewer = false})<CR>]], { noremap = true, silent = true })
			map('n', '<leader>sb', [[<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>]], { noremap = true, silent = true })
			map('n', '<leader>sh', [[<cmd>lua require('telescope.builtin').help_tags()<CR>]], { noremap = true, silent = true })
			map('n', '<leader>st', [[<cmd>lua require('telescope.builtin').tags()<CR>]], { noremap = true, silent = true })
			map('n', '<leader>sd', [[<cmd>lua require('telescope.builtin').grep_string()<CR>]], { noremap = true, silent = true })
			map('n', '<leader>sp', [[<cmd>lua require('telescope.builtin').live_grep()<CR>]], { noremap = true, silent = true })
			map('n', '<leader>so', [[<cmd>lua require('telescope.builtin').tags{ only_current_buffer = true }<CR>]], { noremap = true, silent = true })
			map('n', '<leader>?', [[<cmd>lua require('telescope.builtin').oldfiles()<CR>]], { noremap = true, silent = true })
		end,
	}

	use {
		"hoob3rt/lualine.nvim",
		config = function()
			require('lualine').setup({
				options = {
					theme = 'nord',
					section_separators = '',
					component_separators = '|',
					globalstatus = true,
				},
				sections = {
					lualine_a = {'mode'},
					lualine_b = {'branch'},
					lualine_c = {'filename'},
					lualine_x = {
						{
							'diagnostics',
							sources = {"nvim_diagnostic"},
							symbols = {error = 'E', warn = 'W', info = 'I', hint = 'H'},
							update_in_insert = false,
							always_visible = false,
						},
						{'vim.bo.ff'}, 'encoding', {'vim.bo.filetype == "" and "no ft" or vim.bo.filetype'},
					},
					lualine_y = {'progress'},
					lualine_z = {'location'},
				},
				inactive_sections = {
					lualine_a = {},
					lualine_b = {},
					lualine_c = {'filename'},
					lualine_x = {'location'},
					lualine_y = {},
					lualine_z = {}
				},
			})
		end,
	}

	use {
		"kmarius/lfm.vim",
		requires = {
			{
				"voldikss/vim-floaterm",
				config = [[
				vim.g.floaterm_opener = "edit"
				]],
			}
		},
	}

	use "jasonccox/vim-wayland-clipboard"

	use "junegunn/fzf.vim"

	use {
		"ntpeters/vim-better-whitespace",
		config = [[
		vim.g.better_whitespace_enabled = 0
		vim.g.strip_whitespace_on_save = 1
		vim.g.show_spaces_that_precede_tabs = 1
		vim.g.strip_whitespace_confirm = 0
		vim.g.strip_max_file_size = 10000
		vim.g.better_whitespace_filetypes_blacklist = {"diff", "gitcommit", "qf", "help", "markdown"}
		]],
	}

	use {
		"notjedi/nvim-rooter.lua",
		config = function()
			require('nvim-rooter').setup {
				rooter_patterns = {'.git', '.hg', '.svn', '.luarc.lua', '.clangd'},
				trigger_patterns = {'*'},
				manual = false,
			}
		end
	}

	use {
		"pianocomposer321/yabs.nvim",
		ft = {"rust", "c", "lua"},
		after = "nvim-rooter.lua",
		config = function()
			require("yabs"):setup({
				tasks = {
					rooter_toggle = {
						command = function()
							require("nvim-rooter").rooter_toggle()
						end,
						type = "lua",
					},
				},
				default_task = "rooter_toggle",
				languages = {
					lua = {
						tasks = {
							run = {
								command = "luajit %",
								output = "quickfix",
							},
						},
					},
					rust = {
						tasks = {
							build = {
								command = "cargo build %",
							},
							run = {
								command = "cargo run %",
							},
							release = {
								command = "cargo run --release %",
							},
							test = {
								command = "cargo test %",
							},
						},
					},
				},
			})
			-- run_default_task will actually source a .yabs file and run the corresponding task
			vim.api.nvim_set_keymap("n", "<F4>", "", {callback=function() require('yabs'):run_default_task() end})
			vim.api.nvim_set_keymap("n", "<F5>", "", {callback=function() require('yabs'):run_task('build_and_test') end})
			vim.api.nvim_set_keymap("n", "<F6>", "", {callback=function() require('yabs'):run_task('build_and_run') end})

			local cmd = vim.api.nvim_create_user_command
			cmd("YabsEdit", function()
				local root = require("nvim-rooter").get_root() or "."
				vim.cmd(string.format("edit %s/.yabs", root))
			end, {})

			if require("nvim-rooter").get_root() then
				require("nvim-rooter").rooter()
				require("yabs"):load_config_file()
				require("nvim-rooter").rooter()
			end
		end
	}

	use {
		"https://gitlab.com/yorickpeterse/nvim-pqf",
		config = function()
			require('pqf').setup({
				signs = {
					error = 'E',
					warning = 'W',
					info = 'I',
					hint = 'H'
				},
			})
		end
	}

	use {
		'numToStr/Comment.nvim',
		config = function()
			require('Comment').setup()
		end
	}

	use {
		"windwp/nvim-autopairs",
		config = function()
			require("nvim-autopairs").setup({})
		end,
	}

	use {
		"echasnovski/mini.nvim",
		event = "BufWinEnter",
		config = function()
			require('mini.surround').setup({
				highlight_duration = 250,
			})
			require('mini.cursorword').setup({
				delay = 250,
			})
		end,
	}

	use {
		"lewis6991/gitsigns.nvim",
		requires = {"nvim-lua/plenary.nvim"},
		config = function()
			require("gitsigns").setup()
		end
	}

	use {
		"simnalamburt/vim-mundo",
		config = function()
			vim.api.nvim_set_keymap("n", "<Leader>u",  "<cmd>MundoToggle<cr>", {})
		end,
	}

	use {
		"neomake/neomake",
		ft = {"sh", "tex"},
		config = function()
			local id = vim.api.nvim_create_augroup("neomake", {clear = true})
			vim.api.nvim_create_autocmd("BufReadPost", {
				group = id,
				pattern = "PKGBUILD",
				command = "NeomakeDisableBuffer",
			})
		end
	}

	use {
		"lervag/vimtex",
		ft = {"tex", "latex", "plaintex"},
		config = function ()
			-- vim.g.vimtex_view_automatic_xwin = 0
			vim.g.vimtex_view_zathura_custom_options = ""
			vim.g.vimtex_view_method = "zathura_custom"
			vim.g.vimtex_indent_on_ampersands = 0
			vim.g.vimtex_quickfix_mode = 0
			vim.g.vimtex_indent_enabled = 1
			vim.g.vimtex_fold_enabled = 0
			vim.g.vimtex_completion_enabled = 1
			vim.g.vimtex_compiler_latexmk = {
				backend = "nvim",
				background = 1,
				build_dir = "",
				callback = 1,
				continuous = 1,
				executable = "latexmk",
				hooks = {
					function (msg)
						local m = string.match(msg, "Latexmk: All targets %((.*)%-Lsg%.pdf%) are up%-to%-date")
						if m then
							os.execute("make " .. m .. ".pdf >/dev/null 2>&1 &")
						end
					end,
				},
				options = {
					"-e",
					"-verbose",
					"-file-line-error",
					"-synctex=1",
					"-interaction=nonstopmode",
				},
			}
		end,
	}

	use {
		'kristijanhusak/orgmode.nvim',
		disable = true,
		config = function()
			require('orgmode').setup({
				org_agenda_files = {},
				org_default_notes_file = '~/Documents/notes.md',
			})
		end
	}

	use {
		"simrat39/rust-tools.nvim",
		requires = {
			"neovim/nvim-lspconfig",
			"nvim-lua/plenary.nvim",
			-- "mfussenegger/nvim-dap",
		},
		ft = {"rust"},
		config = function()
			require("rust-tools").setup({
				on_attach = require("lspconfig").on_attach
			})
		end
	}

	use {
		"inkch/vim-fish",
		-- "kmarius/vim-fish",
		-- branch = "test",
		ft = "fish",
	}

	use {
		"fatih/vim-go",
		ft = "go",
		run = ":GoInstallBinaries",
		config = function()
			vim.cmd [[
			set autowrite
			setlocal updatetime=100
			]]

			-- vim.api.nvim_set_keymap("n", "<Leader>lb",  ":GoBuild .<cr>", {})
			-- nmap <buffer> <leader>lb :GoBuild .<CR>
			-- nmap <buffer> <leader>ll :silent !termite -e 'go run .'&<CR>
			-- nmap <buffer> <leader>lr <Plug>(go-run)
			-- nmap <buffer> <leader>lt <Plug>(go-test)
			-- nmap <buffer> <leader>i <Plug>(go-info)
			-- nmap <buffer> <leader>I :GoSameIds<CR>

			vim.g.go_highlight_fields = 1
			vim.g.go_highlight_functions = 1
			vim.g.go_highlight_function_calls = 1
			vim.g.go_highlight_extra_types = 1
			vim.g.go_highlight_operators = 1

			vim.g.go_fmt_autosave = 1
			vim.g.go_fmt_command = "goimports"

			vim.g.go_auto_type_info = 1
		end,
	}

	use "JosefLitos/vim-i3config"

	use "cespare/vim-toml"

	use "petRUShka/vim-opencl"

	use {
		"Firef0x/PKGBUILD.vim",
		ft = "PKGBUILD",
	}
end)
