---@diagnostic disable: undefined-global

local util = require("util")

local M = {}

local fn = vim.fn
local get_vvar = vim.api.nvim_get_vvar

function M.foldtext()
	local lines = get_vvar("foldend") - get_vvar("foldstart") + 1

	local minleft = 30
	local maxleft = 40
	local rightmargin = 5
	local leftpadding = 3
	local prechar = " "
	local postchar = " "

	local res = fn.getline(get_vvar("foldstart"))

	if vim.opt.foldmethod:get() == "marker" then
		local marker = vim.opt.foldmarker:get()[1]
		res = string.gsub(res, util.escape_lua_pattern(marker) .. "%d*", "")
	end

	local strLines = string.format("+%d Lines", lines)

	local left = fn.winwidth(0) - fn.strdisplaywidth(strLines) - rightmargin
	if left < minleft then
		left = minleft
	elseif left > maxleft then
		left = maxleft
	end
	left = left - fn.strdisplaywidth(lines)

	local extraminus = left - fn.strdisplaywidth(res)

	res = res .. string.rep(" ", leftpadding) .. string.rep(prechar, extraminus - 1) .. strLines
	res = res .. string.rep(postchar, fn.winwidth(0) - fn.strdisplaywidth(res))

	return res
end

local function next_non_blank(lnum)
	local numlines = fn.line("$")
	local current = lnum + 1

	while current <= numlines do
		if string.match(fn.getline(current), "%S") then
			return current
		end
		current = current + 1
	end
	return -2
end

local function indent_level(lnum)
	return fn.indent(lnum) / vim.o.shiftwidth
end

function M.get_indent_fold (lnum)
	if string.match(fn.getline(lnum), "^%s*$") then
		return indent_level(next_non_blank(lnum))
	end

	local this_indent = indent_level(lnum)
	local next_indent = indent_level(next_non_blank(lnum))

	if next_indent <= this_indent then
		return this_indent
	else
		return ">" .. next_indent
	end
end

function M.setup()
	vim.o.foldmarker = "{{{,}}}"
	vim.o.foldmethod = "marker"
	vim.o.foldtext = "v:lua.foldtext()"
	vim.o.foldexpr = "v:lua.get_indent_fold(v:lnum)"
	_G.foldtext = M.foldtext
	_G.get_indent_fold = M.get_indent_fold
end

return M
