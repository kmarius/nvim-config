---@diagnostic disable: undefined-global

local fn = vim.fn

local M = {}

function M.hi_syn_info()
	if fn.exists("*synstack") == 0 then
		return
	end
	local stack = fn.synstack(fn.line("."), fn.col("."))
	for i = 1,#stack do
		stack[i] = fn.synIDattr(stack[i], "name")
	end
	print("synstack: ["..table.concat(stack, " ").."]")
	if #stack > 0 then
		vim.cmd("hi "..stack[#stack])
	end
end


function M.header_toggle()
    local filename = vim.fn.expand("%:t")
    if string.match(filename, "%.c$") or string.match(filename, "%.cpp$")then
        vim.cmd("edit %:r.h")
    elseif string.match(filename, "%.h$") then
		if vim.fn.filereadable((string.gsub(filename, "%.h", ".cpp"))) ~= 0 then
			vim.cmd("edit %:r.cpp")
		else
			vim.cmd("edit %:r.c")
		end
    end
end

return M
