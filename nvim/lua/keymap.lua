---@diagnostic disable: undefined-global

local M = {}

function M.setup()
	local map = vim.api.nvim_set_keymap
	-- close all folds but the current one
	map("n", "zV",  "zMzv", {noremap=true})

	map("n", "Q", "gqip", {noremap=true})
	map("v", "Q", "gq", {noremap=true})

	--  treat long lines as break lines
	map("n", "j", "gj", {})
	map("n", "k", "gk", {})
	map("v", "j", "gj", {})
	map("v", "k", "gk", {})

	-- buffers
	map("n", "<Leader>n", ":bn<cr>", {})
	map("v", "<Leader>n", ":bn<cr>", {})
	map("n", "<Leader>p", ":bp<cr>", {})
	map("v", "<Leader>p", ":bp<cr>", {})
	map("n", "<Leader>d", ":bdel<cr>", {})
	map("v", "<Leader>d", ":bdel<cr>", {})

	-- move between windows
	map("n", "<c-h>", "<c-w>h", {})
	map("i", "<c-h>", "<c-w>h", {})
	map("v", "<c-h>", "<c-w>h", {})
	map("n", "<c-j>", "<c-w>j", {})
	map("i", "<c-j>", "<c-w>j", {})
	map("v", "<c-j>", "<c-w>j", {})
	map("n", "<c-k>", "<c-w>k", {})
	map("i", "<c-k>", "<c-w>k", {})
	map("v", "<c-k>", "<c-w>k", {})
	map("n", "<c-l>", "<c-w>l", {})
	map("i", "<c-l>", "<c-w>l", {})
	map("v", "<c-l>", "<c-w>l", {})

	map("n", "<esc>", [[:noh<cr><esc>]], {noremap=true, silent=true})

	map("n", "<Leader>w",  [[:update<cr>]], {})

	-- Space to toggle folds.
	map("n", "<Space>",  "za", {noremap=true})

	map("n", "<c-i>", "", {callback=require("commands").hi_syn_info})
	-- show help for word under cursor (mainly in vimrc)
	-- map("n", "<Leader>h",  [[:exe ':help ' .expand('<cword>')<cr>]], {})
	map("n", "<Leader>h", "", {callback=require("commands").header_toggle})
end

return M
