if exists("b:current_syntax")
	finish
endif

syntax clear

syn region nmdRule start=/^/ end=/$/ oneline contains=nmdRule1,nmdRule2,nmdRule3,nmdCommand nextgroup=nmdRule1
syn match nmdCommand '.*$' contained skipwhite
syn match nmdRule3 '[^ \t]\+' nextgroup=nmdCommand contained skipwhite
syn match nmdRule2 '[^ \t]\+' nextgroup=nmdRule3 contained skipwhite
syn match nmdRule1 '\s*[^ \t]\+' nextgroup=nmdRule2 contained skipwhite

syn match nmdPath '^PATH'
syn match nmdComment '^\s*#.*'

hi link nmdPath Identifier
hi link nmdComment Comment
hi link nmdCommand Statement
hi link nmdRule1 Number
hi link nmdRule2 PreProc
hi link nmdRule3 Type

let b:current_syntax = "nmdtab"
