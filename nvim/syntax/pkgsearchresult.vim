"
" syntax/pkgsearchresult.vim
"

if exists("b:current_syntax")
	finish
endif

syntax clear

syn region pacPackageNotSelected start=/^[ \t#]\+\(core\|extra\|community\|multilib\|custom\|aur\)/ end="$" oneline contains=@pacRepo,pacSomething,pacBase,pacInstalled,pacHash
syn region pacPackageSelected start=/^[ \t]*\(core\|extra\|community\|multilib\|custom\|aur\)/ end="$" oneline contains=@pacRepo,pacSomething,pacBase,pacInstalled
syn match pacDescription  "^#     .*$" contains=pacHash
syn match pacInstalled '\[installed[^\]]*\]'
syn match pacBase '(base)\|(gnome-extra)' contained
syn match pacPackageName '[^ ]*' contained nextgroup=pacPackageVersion
syn match pacPackageVersion ' [^ ]*' contained
syn match pacSomething '([0123456789., ]*)'
syn cluster pacRepo contains=pacRepoCore,pacRepoExtra,pacRepoCommunity,pacRepoMultilib,pacRepoAur,pacRepoCustom
syn match pacHash '^[ \t]*#' contained

syn match pacRepoCore 'core/' nextgroup=pacPackageName
syn match pacRepoExtra 'extra/' nextgroup=pacPackageName
syn match pacRepoCommunity 'community/' nextgroup=pacPackageName
syn match pacRepoMultilib 'multilib/' nextgroup=pacPackageName
syn match pacRepoCustom 'custom/' nextgroup=pacPackageName
syn match pacRepoAur 'aur/' nextgroup=pacPackageName

hi link pacInstalled Function
hi link pacPackageRepo Statement
hi link pacBase Statement

hi pacPackage ctermbg=2 guibg=#008000
hi pacSomething ctermfg=11 cterm=bold guifg=#ffff00 gui=bold
hi pacInstalled ctermfg=11 cterm=bold guifg=#ffff00 gui=bold
hi pacPackageVersion ctermfg=76 guifg=#5fdf00 cterm=bold gui=bold
hi pacRepoAur ctermfg=5 cterm=bold guifg=#ff00ff gui=bold
hi pacRepoCommunity ctermfg=5 cterm=bold guifg=#ff00ff gui=bold
hi pacRepoMultilib ctermfg=5 cterm=bold guifg=#ff00ff gui=bold
hi pacPackageName cterm=bold ctermfg=255 gui=bold guifg=#ffffff
hi pacRepoCore cterm=bold ctermfg=1 gui=bold guifg=#ff0000
hi pacRepoCustom cterm=bold ctermfg=1 gui=bold guifg=#0088cc
hi pacRepoExtra cterm=bold ctermfg=76 guifg=#5fdf00 gui=bold
hi link pacModeLine Comment
hi link pacHash Comment

let b:current_syntax = "pkgsearchresult"
