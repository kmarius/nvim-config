"
" vim syntax file syncthing ignore files
"

if exists("b:current_syntax")
	finish
endif

syntax clear

syn match stComment '^\s*//.*'
syn match stPrefix '(?[id])'
syn match stInclude '^#include .*'
syn match stCharacterClass '\[[^]]*\]'
syn match stQuestionmark '?'
syn match stExclamationmark '!'
syn match stAsterisk '*'

hi link stComment Comment
hi link stInclude PreProc
hi link stPrefix Type
hi link stCharacterClass Type
hi link stQuestionmark Type
hi link stExclamationmark Type
hi link stAsterisk Type

let b:current_syntax = "stignore"
