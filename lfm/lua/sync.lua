-- utilities to synchronize the copy buffer between instances

local shell = require("shell")

local fn = lfm.fn
local pid = fn.getpid()

local M = {}

local setup = false

local function on_startup()
	shell.execute(string.format([=[
	cd /var/run/user/1000/lfm
	for fifo in *.fifo; do
		case $fifo in
		debug.*|%d.*) ;;
		*)
		pid=${fifo%%.fifo}
		if [ -e /proc/$pid ]; then
			echo 'require("sync").load_sync()' >"$fifo"
			exit
			fi
			;;
			esac
			done
			]=], pid), {fork=true, quiet=true, out=false, err=false})

end

function M.load_sync()
	if not setup then
		return
	end
	local mode, load = lfm.fm.load_get()
	shell.execute(string.format([=[
	cd /var/run/user/1000/lfm
	for fifo in *.fifo; do
		case $fifo in
		debug.*|%d.*) ;;
		*)
		pid=${fifo%%.fifo}
		if [ -e /proc/$pid ]; then
			cat >"$fifo" <<-EOF &
			lfm.fm.load_set('%s', {%s})
			lfm.ui.draw()
			EOF
			fi
			;;
			esac
			done
			]=], pid, mode, shell.escape(load, ", ")), {fork=true, quiet=true, out=false, err=false})
end

local load_clear

-- not called somehow
function M.load_clear(nosync)
	load_clear()
	if not nosync then
		M.load_sync()
	end
end

function M.setup()
	if not load_clear then
		load_clear = lfm.fm.load_clear
	end
	lfm.fm.load_clear = M.load_clear
	lfm.register_hook("LfmEnter", on_startup)
	setup = true
end

return M
