local lfm = lfm

local fm = lfm.fm

local M = {}

local current = 1
local tablist = {os.getenv("PWD")}

function M.print()
	local t = {}
	for i = 1,#tablist do
		if tablist[i] then
			if i == current then
				table.insert(t, "|"..current.."|")
			else
				table.insert(t, " "..i.." ")
			end
		end
	end
	print("tabs: " .. table.concat(t))
	-- we should probably only clear our own message here, i.e. get an ID
	-- on print, and pass it to message_clear
	lfm.schedule(lfm.message_clear, 5000)
end

function M.new()
	for i = 1,#tablist+1 do
		if not tablist[i] then
			tablist[i] = os.getenv("PWD")
			tablist[current] = os.getenv("PWD")
			current = i
			M.print()
			return
		end
	end
end

function M.next()
	for i = 1,#tablist do
		local j = (current + i - 1) % #tablist + 1
		if j == current then
			return
		end
		if tablist[j] then
			tablist[current] = os.getenv("PWD")
			current = j
			fm.chdir(tablist[j])
			M.print()
			return
		end
	end
	M.print()
end

function M.prev()
	for i = 1,#tablist do
		local j = (#tablist + current - i - 1) % #tablist + 1
		if tablist[j] then
			tablist[current] = os.getenv("PWD")
			current = j
			fm.chdir(tablist[j])
			M.print()
			return
		end
	end
	M.print()
end

function M.close()
	local i = 0
	for j = current+1,#tablist do
		if tablist[j] then
			i = j
			break
		end
	end
	if i == 0 then
		for j = current-1,1,-1 do
			if tablist[j] then
				i = j
				break
			end
		end
	end
	if i == 0 then
		lfm.quit()
	else
		tablist[current] = nil
		current = i
		fm.chdir(tablist[current])
		M.print()
	end
end

return M
