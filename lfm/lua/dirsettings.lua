local lfm = lfm

local M = {}

local applied = {}
local settings

local function on_chdir()
	local dir = lfm.fn.getpwd()
	if not settings[dir] or applied[dir] then
		return
	end
	settings[dir]()
	applied[dir] = true
end

function M.setup(t)
	t = t or {}
	settings = t
	lfm.register_hook("ChdirPost", on_chdir)
end

return M
