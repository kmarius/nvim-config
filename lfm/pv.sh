#!/bin/bash

set -C -f -u
IFS=$'\n' # bashism

# ANSI color codes are supported.
# STDIN is disabled, so interactive scripts won't work properly

# This script is considered a configuration file and must be updated manually.

# Meanings of exit codes:
# code | meaning    | action of ranger
# -----+------------+-------------------------------------------
# 0    | success    | Display stdout as preview
# 1    | no preview | Display no preview at all
# 2    | plain text | Display the plain content of the file

# Script arguments
FILE_PATH="${1}"         # Full path of the highlighted file

FILE_EXTENSION="${FILE_PATH##*.}"
FILE_EXTENSION_LOWER=$(echo "${FILE_EXTENSION}" | tr '[:upper:]' '[:lower:]')

# Settings
ARCHIVE_SIZE_MAX=4294967296  #4GiB
HIGHLIGHT_SIZE_MAX=262143  # 256KiB
HIGHLIGHT_TABWIDTH=4
HIGHLIGHT_STYLE='molokai'
PYGMENTIZE_STYLE='autumn'

if [ "$(stat --file-system --format=%T "$FILE_PATH")" == "fuseblk" ]; then
	echo "preview disabled on fuseblk"
    exit 1
fi

# show info for pacman packages
pkginfo() {
	if echo "$1" | grep -q '\.pkg\.tar' ; then
		tar -xf "$1" .PKGINFO --occurrence -O 2>/dev/null \
			| awk '/depend/{exit 0} /^#/{next;} 1'
	else
		return 1
	fi
}

handle_extension() {
    case "${FILE_EXTENSION_LOWER}" in
        # Archive
        a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|\
        rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zst|zip)
            if [ "$( stat --printf='%s' -- "${FILE_PATH}" )" -gt "${ARCHIVE_SIZE_MAX}" ]; then
				# prevent bsdtar from eating up memory
				stat "${FILE_PATH}"
				exit 1
            fi
			pkginfo "${FILE_PATH}" && exit 1
            atool --list -- "${FILE_PATH}" && exit 1
            bsdtar --list --file "${FILE_PATH}"
            exit 1;;
        rar)
            # Avoid password prompt by providing empty password
            unrar lt -p- -- "${FILE_PATH}"
            exit 1;;
        7z)
            # Avoid password prompt by providing empty password
            7z l -p -- "${FILE_PATH}"
            exit 1;;

        # PDF
        pdf)
            # Preview as text conversion
            pdftotext -l 10 -nopgbrk -q -- "${FILE_PATH}" -
            mutool draw -F txt -i -- "${FILE_PATH}" 1-10
            exiftool "${FILE_PATH}"
            exit 1;;

        # BitTorrent
        torrent)
            transmission-show -- "${FILE_PATH}"
            exit 1;;

        # OpenDocument
        odt|ods|odp|sxw)
            # Preview as text conversion
            odt2txt "${FILE_PATH}"
            exit 1;;

        # HTML
        htm|html|xhtml)
            # Preview as text conversion
            w3m -dump "${FILE_PATH}"
            lynx -dump -- "${FILE_PATH}"
            elinks -dump "${FILE_PATH}"
            ;; # Continue with next handler on failure
		json)
			jq '' "${FILE_PATH}"
			exit 1
			;;
		md)
			glow -s dark "${FILE_PATH}"
			;;
    esac
}

handle_mime() {
    mimetype="${1}"
    case "${mimetype}" in
        # Text
        text/* | */xml)
            # Syntax highlight
            if [ "$( stat --printf='%s' -- "${FILE_PATH}" )" -gt "${HIGHLIGHT_SIZE_MAX}" ]; then
                exit 2
            fi
            if [ "$( tput colors )" -ge 256 ]; then
                pygmentize_format='terminal256'
                highlight_format='xterm256'
            else
                pygmentize_format='terminal'
                highlight_format='ansi'
            fi
			if [ "${FILE_PATH##*/}" = PKGBUILD ]; then
				syntax='--syntax=sh'
			fi
			# HIGHLIGHT NEEDS TO BE IN THE SAME DIRECTORY AS THE FILE OTHERWISE
			# THE SYNTAX DETECTION WILL NOT WORK EVEN WITH ./ IN FROM IT JUST
			# FUCKING BREAKS. CHECK THIS FOR EXAMPLE LOL
			# FILE_PATH=$(realpath "$FILE_PATH")
            highlight --replace-tabs="${HIGHLIGHT_TABWIDTH}" \
				--out-format="${highlight_format}" \
				--style="${HIGHLIGHT_STYLE}" \
				--force ${syntax:-} \
				--line-number-length 2 \
				--line-numbers \
				-- "${FILE_PATH}"
            # pygmentize -f "${pygmentize_format}" -O "style=${PYGMENTIZE_STYLE}" -- "${FILE_PATH}"
            exit 2;;
		application/json)
			jq '' "${FILE_PATH}"
			exit 1
			;;
        # Image
        image/*)
            # Preview as text conversion
            # img2txt --gamma=0.6 -- "${FILE_PATH}" && exit 1
            exiftool "${FILE_PATH}"
            exit 1;;

        # Video and audio
        video/* | audio/*|application/octet-stream)
            mediainfo "${FILE_PATH}" && exit 1
            exiftool "${FILE_PATH}"
            exit 1;;
    esac
}

handle_fallback() {
    echo '----- File Type Classification -----' && file --dereference --brief -- "${FILE_PATH}"
    exit 1
}


MIMETYPE="$( file --dereference --brief --mime-type -- "${FILE_PATH}" )"
handle_extension
handle_mime "${MIMETYPE}"
handle_fallback

exit 1
